<?php
Route::get('/', function(){return view('layout');});
Route::get('/series', 'SeriesController@index');
Route::get('/series/criar', 'SeriesController@create');
Route::post('/series/criar', 'SeriesController@store');
Route::delete('/series/{id}', 'SeriesController@destroy');

Route::get('/series/{serieId}/temporadas', 'TemporadasController@index');

