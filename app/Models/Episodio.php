<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Episodio extends Model
{
    protected $table = 'episodios';
    protected $fillable = ['numero'];
    public $timestamps = false;
        
    public function Temporada()
    {
        return $this->belongsTo(Temporada::class);
    }
}
