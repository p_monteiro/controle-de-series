<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Temporada extends Model
{
    protected $table = 'temporadas';
    protected $fillable = ['numero'];
    public $timestamps = false;
    
    public function Episodio()
    {
        return $this->hasMany(Episodio::class);
    }
    public function Serie()
    {
        return $this->belongsTo(Serie::class);
    }
}
