<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Series;
use App\Models\Serie;
use App\Models\Temporada;
use App\Models\Episodio;

class SeriesController extends Controller
{
    public function index(Request $request)
    {
        $series = Serie::query()->orderBy('nome')->get();

        $mensagem = $request->session()->get('mensagem');

        return view('series.index', compact('series', 'mensagem'));
    }

    public function create()
    {
        return view('series.create');
    }

    public function store(Series $request)
    {
        $serie = Serie::create($request->all());
        $temporadas = $request->temporadas;
        for($i=1 ; $i < $temporadas; $i++){
        $temporada = $serie->Temporada()->create(['numero' => $i]);

            for($e = 1; $e <= $request->episodios; $e++ ){
                $temporada->Episodio()->create(['numero' => $e]);
            }
        }
        $request->session()
        ->flash(
            'mensagem',
            "Série {$serie->id} e suas temporadas e episódios criada com sucesso {$serie->nome}"
        );

        return redirect('/series');
    }

    public function destroy(Request $request)
    {
        $serie = Serie::destroy($request->id);

        $request->session()
        ->flash(
            'mensagem',
            "Série removida com sucesso"
        );

        return redirect('/series');
    }
}
