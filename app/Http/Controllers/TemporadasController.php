<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Temporada;
use App\Models\Serie;

class TemporadasController extends Controller
{
    public function index($serieId)
    {
        $temporadas = Serie::find($serieId)->temporada;

        return view('temporadas.index', compact('temporadas'));
    }
}
