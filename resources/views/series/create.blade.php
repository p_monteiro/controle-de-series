@extends('layout')

@section('titulo')
Adicionar Série
@endsection

@section('conteudo')
<form method="post">
    @csrf
    <div class="row">
        <div class="col col-8">
            <label for="nome">Nome</label>
            <input type="text" name="nome" class="form-control">
        </div>
        <div class="col col-2">
            <label for="temporada">Nº Temporadas</label>
            <input type="number" name="temporadas" class="form-control">
        </div>
        <div class="col col-2">
            <label for="episodio">Nº Episódios</label>
            <input type="number" name="episodios" class="form-control">
        </div>
    </div>
    <div class="row">
        <button class="btn btn-primary mt-2 ml-3">Adicionar</button>
    </div>
</form>
@endsection