@extends('layout')

@section('titulo')
Séries
@endsection

@section('conteudo')
@if(!empty($mensagem))
<div class="alert alert-success">
    {{ $mensagem }}
</div>
@endif
<a href="series/criar" class="btn btn-dark mb-2">Adicionar</a>

<ul class="list-group">
    @foreach($series as $serie)
    <li class="list-group-item d-flex justify-content-between align-items-center">{{ $serie->nome }}
        <div class="d-flex">
        <a href="/series/{{ $serie->id }}/temporadas" class="btn btn-info btn-sm mr-1">
                Editar
            </a>
            <form action="/series/{{ $serie->id }}" onsubmit="return confirm('Tem certeza que deseja excluir esta série?')" method="post">
                @csrf
                @method('delete')
                <button class="btn btn-sm btn-danger">Excluir</button>
            </form>
        </div>
    </li>
    @endforeach
</ul>

@endsection