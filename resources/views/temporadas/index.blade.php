@extends('layout')

@section('titulo')
Temporadas
@endsection

@section('content')
<ul class="list-group">
    @foreach($temporadas as $temporada)
    <li class="list-group-item">{{ $temporada->numero }}</li>
    <li class="list-group-item">{{ $temporada->id }}</li>
    @endforeach
</ul>
@endsection